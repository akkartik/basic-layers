Example project that allows newcomers to gradually learn about its internals.
It allows newcomers to build subsets of its features so that they can focus on
its core skeleton at first, later adding features at their own pace.

#### Try it out

First build the project:

```
./build
```

Try running the tests:

```
$ ./a.out test
```

Now try building and running tests for subsets of layers:

```
$ ./build_and_test_until 000*
$ ./build_and_test_until 001*
$ ./build_and_test_until 002*
$ ./build_and_test_until 003*
$ ./build_and_test_until 004*
```

Each command builds all .cc files that start with a numeric prefix and are
lexically less then or equal to its argument. For example, building until
`001*` includes two layers: `000organization.cc` and `001test.cc`.

In a well-formed codebase based on layers, all such subsets of layers should
pass all their tests.

Later layers override earlier ones using _directives_ of the form `:(...)`.
See http://akkartik.name/post/wart-layers for more details, and tangle/Readme.md
in this repo for a reference of supported directives.

#### Coda

This repo is pulls together several unconventional ideas:

1. A zero-dependency build system. See https://git.sr.ht/~akkartik/basic-build
for details.

1. A minimal test harness for C. See https://git.sr.ht/~akkartik/basic-test
for details.

1. Primitives for automatic _white-box_ testing, a more comprehensive and
flexible way to express automated tests. See https://git.sr.ht/~akkartik/basic-whitebox-test
for details.

For a more fully fleshed out codebase using all these ideas, check out https://github.com/akkartik/mu
