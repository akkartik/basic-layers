// First implementation of the example program at https://git.sr.ht/~akkartik/basic-whitebox-test/tree/master/x.cc

int run(int x) {
  trace(0, "app") << "transforming " << x << end();
  int g(int);  // prototype
  int y = g(x);
  int z = 2*y;
  trace(0, "app") << x << " transformed to " << z << end();
  return z;
}

int g(int x) {
  int y = x+1;
  trace(1, "app") << x << " + 1 is " << y << end();
  return y;
}

void test_1() {
  run(3);
  CHECK_TRACE_CONTENTS(
      "app: transforming 3\n"
      "app: 3 + 1 is 4\n"
      "app: 3 transformed to 8\n");
}
