//: The goal of layers is to make programs more easy to understand and more
//: malleable, easy to rewrite in radical ways without accidentally breaking
//: some corner case. Tests further both goals. They help understandability by
//: letting one make small changes and get feedback. What if I wrote this line
//: like so? What if I removed this function call, is it really necessary?
//: Just try it, see if the tests pass. Want to explore rewriting this bit in
//: this way? Tests put many refactorings on a firmer footing.
//:
//: But the usual way we write tests seems incomplete. Refactorings tend to
//: work in the small, but don't help with changes to function boundaries. If
//: you want to extract a new function you have to manually test-drive it to
//: create tests for it. If you want to inline a function its tests are no
//: longer valid. In both cases you end up having to reorganize code as well as
//: tests, an error-prone activity.
//:
//: In response, this layer introduces the notion of domain-driven *white-box*
//: testing. We focus on the domain of inputs the whole program needs to
//: handle rather than the correctness of individual functions. All white-box
//: tests invoke the program in a single way: by calling run() with some
//: input. As the program operates on the input, it traces out a list of
//: _facts_ deduced about the domain:
//:   trace("label") << "fact 1: " << val;
//:
//: Tests can now check for these facts in the trace:
//:   CHECK_TRACE_CONTENTS("label", "fact 1: 34\n"
//:                                 "fact 2: 35\n");
//:
//: Since we never call anything but the run() function directly, we never have
//: to rewrite the tests when we reorganize the internals of the program. We
//: just have to make sure our rewrite deduces the same facts about the domain,
//: and that's something we're going to have to do anyway.
//:
//: To avoid the combinatorial explosion of integration tests, each layer
//: mainly logs facts to the trace with a common *label*. All tests in a layer
//: tend to check facts with this label. Validating the facts logged with a
//: specific label is like calling functions of that layer directly.
//:
//: To build robust tests, trace facts about your domain rather than details of
//: how you computed them.
//:
//: More details: http://akkartik.name/blog/tracing-tests
//:
//: ---
//:
//: Between layers and domain-driven testing, programming starts to look like a
//: fundamentally different activity. Instead of focusing on a) superficial,
//: b) local rules on c) code [like say http://blog.bbv.ch/2013/06/05/clean-code-cheat-sheet],
//: we allow programmers to engage with the a) deep, b) global structure of
//: the c) domain. If you can systematically track discontinuities in the
//: domain, you don't care if the code used gotos as long as it passed all
//: tests. If tests become more robust to run, it becomes easier to try out
//: radically different implementations for the same program. If code is
//: super-easy to rewrite, it becomes less important what indentation style it
//: uses, or that the objects are appropriately encapsulated, or that the
//: functions are referentially transparent.
//:
//: Instead of plumbing, programming becomes building and gradually refining a
//: map of the environment the program must operate under. Whether a program
//: is 'correct' at a given point in time is a red herring; what matters is
//: avoiding regression by monotonically nailing down the more 'eventful'
//: parts of the terrain. It helps readers new and old, and rewards curiosity,
//: to organize large programs in self-similar hierarchies of example tests
//: colocated with the code that makes them work.
//:
//:   "Programming properly should be regarded as an activity by which
//:   programmers form a mental model, rather than as production of a program."
//:   -- Peter Naur (http://alistair.cockburn.us/ASD+book+extract%3A+%22Naur,+Ehn,+Musashi%22)

//:: Core interface

:(before "End Includes")
// Add to the trace (in production code) {

// Example usage:
//   trace(2, "abc") << "line 1" << end();
//
// This call emits this line to the trace:
//   "2 abc: line 1"
//
// The label is a namespace for assertions.
//
// The depth is an indicator of importance, helpful for hiding irrelevant
// details in tools. It is not used in tests.

#define trace(depth, layer)  !trace_stream \
    ? /*print nothing; all args attach to the other branch*/std::cerr \
    : trace_stream->stream(depth, layer)
// }

// Assertions on the trace (in tests) {

// Check that the trace contains some sequence of lines, in sequence, though
// possibly with other lines mixed in.
// Lines are separated by newlines.
//
// Each line is of the form "label: contents".
//
// Example usage:
// To check for the example above:
//   CHECK_TRACE_CONTENTS("abc: line 1\n")
#define CHECK_TRACE_CONTENTS(...)  check_trace_contents(__FUNCTION__, __FILE__, __LINE__, __VA_ARGS__)

// Check that the trace doesn't contain a single line.
#define CHECK_TRACE_DOESNT_CONTAIN(...)  CHECK(trace_doesnt_contain(__VA_ARGS__))

// Check that a trace contains a fixed number of lines with a given label.
#define CHECK_TRACE_COUNT(label, count) \
  if (Passed && trace_count(label) != (count)) { \
    cerr << "\nF - " << __FUNCTION__ << "(" << __FILE__ << ":" << __LINE__ << "): trace_count of " << label << " should be " << count << '\n'; \
    cerr << "  got " << trace_count(label) << '\n';  /* multiple eval */ \
    cerr << trace_stream->readable_contents(label); \
    Passed = false; \
    return;  /* Currently we stop at the very first failure. */ \
  }
// }

//:: Core data structures

:(before "End Types")
struct TraceLine {
  string contents;
  string label;
  int depth;  // 0 is 'sea level'; positive integers are progressively 'deeper' and lower level
  TraceLine(string c, string l, int d);
};

struct TraceStream {
  vector<TraceLine> past_lines;
  // accumulator for current trace_line
  ostringstream* curr_stream;
  string curr_label;
  int curr_depth;
  // other stuff

  TraceStream();
  // start accumulating to a new trace line
  ostream& stream(int depth, string label);
  // finalize the trace line most recently started
  void newline();
  // extract lines matching a given label
  // empty label matches all lines
  string readable_contents(string label);
};

:(before "End Globals")
TraceStream* trace_stream = NULL;
// Trace depths can go from 0 to MAX_DEPTH (both inclusive)
const int MAX_DEPTH = 9999;
std::ofstream trace_file;

:(code)
// start accumulating to a new trace line
ostream& TraceStream::stream(int depth, string label) {
  curr_stream = new ostringstream;
  curr_label = label;
  curr_depth = depth;
  return *curr_stream;
}

// finalize the trace line most recently started
void TraceStream::newline() {
  string trim(const string& s);
  if (!curr_stream) return;
  string curr_contents = curr_stream->str();
  if (!curr_contents.empty()) {
    past_lines.push_back(TraceLine(curr_contents, trim(curr_label), curr_depth));  // preserve indent in contents
    if (trace_file)
      trace_file << std::setw(4) << curr_depth << ' ' << curr_label << ": " << curr_contents << '\n';
  }

  // clean up
  delete curr_stream;
  curr_stream = NULL;
  curr_label.clear();
  curr_depth = MAX_DEPTH;
}

TraceLine::TraceLine(string c, string l, int d) {
  contents = c;
  label = l;
  depth = d;
}

TraceStream::TraceStream() {
  curr_stream = NULL;
  curr_depth = MAX_DEPTH;
}

//: Some syntax for finalizing trace lines
//:  trace(...) << ... << end();

:(before "End Types")
// Passing any object of this type to any ostream stops adding to the current
// trace line.
struct end {};
:(code)
ostream& operator<<(ostream& os, end /*unused*/) {
  if (trace_stream) trace_stream->newline();
  return os;
}

//: Making assertions on the trace

//: first clear trace_stream before every test
:(before "End Reset")
if (trace_stream) delete trace_stream;
trace_stream = new TraceStream;

:(code)
bool check_trace_contents(string FUNCTION, string FILE, int LINE, string expected) {
  if (!Passed) return false;
  if (!trace_stream) return false;
  vector<string> expected_lines = split(expected, "\n");
  int curr_expected_line = 0;
  while (curr_expected_line < expected_lines.size() && expected_lines.at(curr_expected_line).empty())
    ++curr_expected_line;
  if (curr_expected_line == expected_lines.size()) return true;
  string label, contents;
  split_label_contents(expected_lines.at(curr_expected_line), &label, &contents);
  for (vector<TraceLine>::iterator p = trace_stream->past_lines.begin();  p != trace_stream->past_lines.end();  ++p) {
    if (label != p->label) continue;
    if (contents != trim(p->contents)) continue;
    ++curr_expected_line;
    while (curr_expected_line < expected_lines.size() && expected_lines.at(curr_expected_line).empty())
      ++curr_expected_line;
    if (curr_expected_line == expected_lines.size()) return true;
    split_label_contents(expected_lines.at(curr_expected_line), &label, &contents);
  }

  if (line_exists_anywhere(label, contents)) {
    cerr << "\nF - " << FUNCTION << "(" << FILE << ":" << LINE << "): line [" << label << ": " << contents << "] out of order in trace:\n";
    cerr << trace_stream->readable_contents("");
  }
  else {
    cerr << "\nF - " << FUNCTION << "(" << FILE << ":" << LINE << "): missing [" << contents << "] in trace:\n";
    cerr << trace_stream->readable_contents(label);
  }
  Passed = false;
  return false;
}

string TraceStream::readable_contents(string label) {
  string trim(const string& s);  // prototype
  ostringstream output;
  label = trim(label);
  for (vector<TraceLine>::iterator p = past_lines.begin();  p != past_lines.end();  ++p)
    if (label.empty() || label == p->label)
      output << std::setw(4) << p->depth << ' ' << p->label << ": " << p->contents << '\n';
  return output.str();
}

bool trace_doesnt_contain(string expected) {
  vector<string> tmp = split_first(expected, ": ");
  if (tmp.size() == 1) {
    cerr << expected << ": missing label or contents in trace line\n";
    exit(1);
  }
  return trace_count(tmp.at(0), tmp.at(1)) == 0;
}

int trace_count(string label, string line) {
  if (!trace_stream) return 0;
  long result = 0;
  for (vector<TraceLine>::iterator p = trace_stream->past_lines.begin();  p != trace_stream->past_lines.end();  ++p) {
    if (label == p->label) {
      if (line == "" || trim(line) == trim(p->contents))
        ++result;
    }
  }
  return result;
}

int trace_count_prefix(string label, string prefix) {
  if (!trace_stream) return 0;
  long result = 0;
  for (vector<TraceLine>::iterator p = trace_stream->past_lines.begin();  p != trace_stream->past_lines.end();  ++p) {
    if (label == p->label) {
      if (starts_with(trim(p->contents), trim(prefix)))
        ++result;
    }
  }
  return result;
}

void split_label_contents(const string& s, string* label, string* contents) {
  static const string delim(": ");
  size_t pos = s.find(delim);
  if (pos == string::npos) {
    *label = "";
    *contents = trim(s);
  }
  else {
    *label = trim(s.substr(0, pos));
    *contents = trim(s.substr(pos+delim.size()));
  }
}

bool line_exists_anywhere(const string& label, const string& contents) {
  for (vector<TraceLine>::iterator p = trace_stream->past_lines.begin();  p != trace_stream->past_lines.end();  ++p) {
    if (label != p->label) continue;
    if (contents == trim(p->contents)) return true;
  }
  return false;
}

// helpers

// strip whitespace at start and end of a string
string trim(const string& s) {
  string::const_iterator first = s.begin();
  while (first != s.end() && isspace(*first))
    ++first;
  if (first == s.end()) return "";
  string::const_iterator last = --s.end();
  while (last != s.begin() && isspace(*last))
    --last;
  ++last;
  return string(first, last);
}

vector<string> split(string s, string delim) {
  vector<string> result;
  size_t begin=0, end=s.find(delim);
  while (true) {
    if (end == string::npos) {
      result.push_back(string(s, begin, string::npos));
      break;
    }
    result.push_back(string(s, begin, end-begin));
    begin = end+delim.size();
    end = s.find(delim, begin);
  }
  return result;
}

vector<string> split_first(string s, string delim) {
  vector<string> result;
  size_t end=s.find(delim);
  result.push_back(string(s, 0, end));
  if (end != string::npos)
    result.push_back(string(s, end+delim.size(), string::npos));
  return result;
}

:(before "End Includes")
#include <vector>
using std::vector;
#include <iostream>
using std::ostream;
#include <fstream>
using std::ofstream;
#include <sstream>
using std::ostringstream;
#include <iomanip>
