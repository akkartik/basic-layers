// Second possible implementation of the example program at https://git.sr.ht/~akkartik/basic-whitebox-test/tree/master/x.cc
//
// This is just a demonstration of the mechanisms provided, not an example of
// good taste. In real projects refactorings should just modify the layer
// involved, rather than take up a new layer.

:(replace{} "int run(int x)")
int run(int x) {
  trace(0, "app") << "transforming " << x << end();
  int y = x+1;
  trace(1, "app") << x << " + 1 is " << y << end();
  int z = 2*y;
  trace(0, "app") << x << " transformed to " << z << end();
  return z;
}

//: No new tests, but the test defined in the previous layer continues to run
//: and pass.
